﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHandler : MonoBehaviour {

    public static GameHandler Game;

    private char[] Alfabet = { 'a', 'ą', 'b', 'c', 'ć', 'd', 'e', 'ę', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'ł', 'm', 'n', 'ń', 'o', 'ó', 'p', 'r', 's', 'ś', 't', 'u', 'w', 'y', 'z', 'ź', 'ż' };
    private int dlugoscslowa;
    private int zgadnietychliter;
    private int zyc;
    private string szukaneslowo;
    public string SLOWO {
        get
        {
            return szukaneslowo;
        }
        set
        {
            szukaneslowo = value;
            dlugoscslowa = szukaneslowo.Length;
            //wykreskować słowo
            Display.Pokaz.Kreskuj(dlugoscslowa);
        }
    }

    // Use this for initialization
    void Awake()
    {
        Game = this;
        zyc = 11;
        zgadnietychliter = 0;
    }

    public void SprawdzLitere(int id)
    {
        char litera= Alfabet[id];
        int znalezionych = 0;
        for(int i=0; i < dlugoscslowa; i++)
        {
            if (szukaneslowo[i] == litera)
            {
                Display.Pokaz.WpiszLitere(i, litera);
                znalezionych += 1;
            }
        }
        if (znalezionych == 0)
        {
            zyc -= 1;
            Display.Pokaz.Szczebelek(zyc);
            if (zyc <= 0)
            {
                Display.Pokaz.EndGame(false);
            }
        }
        else
        {
            zgadnietychliter += znalezionych;
            if (zgadnietychliter >= dlugoscslowa)
            {
                Display.Pokaz.EndGame(true);
            }
        }
    }


	
}
