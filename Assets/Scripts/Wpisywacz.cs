﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Wpisywacz : MonoBehaviour {
    public int id;
    public static int liczbaliter=0;
    private Text Literka;
    private char[] Alfabet = { 'a', 'ą', 'b', 'c', 'ć', 'd', 'e', 'ę', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'ł', 'm', 'n', 'ń', 'o', 'ó', 'p', 'r', 's', 'ś', 't', 'u', 'w', 'y', 'z', 'ź', 'ż' };
    private bool wcisniety = false;

    private void Start()
    {
        id = liczbaliter;
        liczbaliter += 1;
        
        Literka = gameObject.GetComponent<Text>();
        Literka.rectTransform.position = new Vector2(Screen.height * 0.07f + id % 11 *Screen.width* 0.09f, Screen.height *0.3f - Screen.height*0.11f * (int)(id / 11));
        Literka.color = new Color32(0, 0, 0, 255);
        Literka.text = null;
        Literka.text += Alfabet[id];
    }

    public void Kliknij()
    {
        if (!wcisniety)
        {
            wcisniety = true;
            Literka.color = new Color32(120, 120, 120, 255);
            GameHandler.Game.SprawdzLitere(id);
        }
    }
}
