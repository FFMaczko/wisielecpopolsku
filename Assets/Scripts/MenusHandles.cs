﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenusHandles : MonoBehaviour {

    public void SinglePlayer()
    {
        SceneManager.LoadScene("SinglePlayer");
        Wpisywacz.liczbaliter = 0;
    }

    public void MultiPlayer()
    {
        SceneManager.LoadScene("MultiPlayer");
        Wpisywacz.liczbaliter = 0;
    }

    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void MoreGames()
    {
        Application.OpenURL("https://play.google.com/store/apps/developer?id=Filip+M%C4%85czko&gl=PL");
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Menu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void OdNowa()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Wpisywacz.liczbaliter = 0;
    }
}
