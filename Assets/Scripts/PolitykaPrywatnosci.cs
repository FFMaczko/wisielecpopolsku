﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PolitykaPrywatnosci : MonoBehaviour
{
    public const string PRIVACY_PREF = "Privacy";
    public const int PRIVACY_POLICY_VERSION = 1;
    public Canvas PrivacyMessage;
    public Canvas Menu;
    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey(PRIVACY_PREF))
        { 
            PlayerPrefs.SetInt(PRIVACY_PREF, 0);
        }
        if (PlayerPrefs.GetInt(PRIVACY_PREF) < PRIVACY_POLICY_VERSION)
        {
            Menu.enabled = false;
            PrivacyMessage.enabled = true;
        }
    }


    public void AcceptTerms()
    {
        PlayerPrefs.SetInt(PRIVACY_PREF, PRIVACY_POLICY_VERSION);
        Menu.enabled = true;
    }

    public void Decline()
    {
        PlayerPrefs.SetInt(PRIVACY_PREF, 0);
        Application.Quit();
    }

    public void GoToPrivacyPolicy()
    {
        Application.OpenURL("https://filipmaczko.pl/ppWisielec.html");
    }
}
