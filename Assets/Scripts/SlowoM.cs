﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlowoM : MonoBehaviour {
    private char[] Alfabet = { 'a', 'ą', 'b', 'c', 'ć', 'd', 'e', 'ę', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'ł', 'm', 'n', 'ń', 'o', 'ó', 'p', 'r', 's', 'ś', 't', 'u', 'w', 'y', 'z', 'ź', 'ż' };
    public Canvas Wprowadzanie;
    public Text Uwagi;
    public InputField wpisane;
    private string wprowadzoneslowo;

    public void Wprowadz()
    {
        bool SlowoOK=true;
        wprowadzoneslowo = wpisane.text;
        wprowadzoneslowo = wprowadzoneslowo.ToLower();
        for(int i = 0; i < wprowadzoneslowo.Length; i++)
        {
            if (!SprawdzZnak(wprowadzoneslowo[i]))
            {
                SlowoOK = false;
                break;
            }
        }
        if (SlowoOK)
        {
            GameHandler.Game.SLOWO = wprowadzoneslowo;
            Wprowadzanie.enabled = false;
        }
        else
        {
            Uwagi.text = "We wprowadzonym tekście znajdują się znaki spoza alfabetu. Popraw to i kliknij wprowadź"; 
        }
    }

    private bool SprawdzZnak(char c)
    {
        for (int i = 0; i < Alfabet.Length; i++)
        {
            if (c == Alfabet[i])
            {
                return true;
            }
        }
        return false;
    }
}
