﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Display : MonoBehaviour {

    public Text slowo;
    public Image wisielec;
    public Canvas EndScreen;
    public Text EndText;
    public Image EndImage;

    public Sprite HappyFace;
    public Sprite SadFace;
    public Sprite[] obrazki;

    public static Display Pokaz;

    private void Awake()
    {
        Pokaz = this;
        EndScreen.enabled = false;
    }

    public void Kreskuj(int liczba)
    {
        slowo.text = null;
        for(int i=0; i < liczba; i++)
        {
            slowo.text += '-';
        }
    }

    public void WpiszLitere(int i, char c)
    {
        string wyswietlaneslowo = null;
        for(int j = 0; j < slowo.text.Length; j++)
        {
            if (j != i)
            {
                wyswietlaneslowo += slowo.text[j];
            }
            else
            {
                wyswietlaneslowo += c;
            }
        }
        slowo.text = wyswietlaneslowo;
    }

    public void Szczebelek(int i)
    {
        wisielec.sprite = obrazki[i];
        wisielec.SetNativeSize();
    }

    public void EndGame(bool win)
    {
        EndScreen.enabled = true;
        if (win)
        {
            EndImage.sprite = HappyFace;
            EndText.text = "Wygrałeś !!!\n Szukane słowo:" + GameHandler.Game.SLOWO;
        }
        else
        {
            EndImage.sprite = SadFace;
            EndText.text = "Przegrałeś \n Szukane słowo:"+GameHandler.Game.SLOWO;
        }
        EndImage.SetNativeSize();
    }
}
